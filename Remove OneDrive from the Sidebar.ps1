#Removes the OneDrive Link from the Windows 10 Sidebar
$confirmation = Read-Host 'This script will modify registry values on your computer. Are you Sure You Want To Proceed? (y/n)'
    if ($confirmation -eq 'y') {
        $registryPath = "Registry::HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}"
        $Name = "System.IsPinnedToNameSpaceTree"
        $value = "0"
        New-ItemProperty -Path $registryPath -Name $name -Value $value `
        -PropertyType DWORD -Force | Out-Null
        pause
    }
